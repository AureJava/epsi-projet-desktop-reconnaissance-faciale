import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.*;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.opencv.core.Core;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * The main class for a JavaFX application. It creates and handle the main
 * window with its resources (style, graphics, etc.).
 *
 * This application handles a video stream and try to find any possible human
 * face in a frame. It can use the Haar or the LBP classifier.
 *
 * @author <a>Aurelien</a>
 * @version 2.0 (2017-03-10)
 * @since 1.0 (2014-01-10)
 *
 */

public class Main extends Application
{
    @Override
    public void start(Stage primaryStage) throws Exception
    {
        try
        {
            // load the FXML resource
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/sample.fxml"));
            //Parent root2=(Parent) loader.load();
            //Parent root2 = FXMLLoader.load(getClass().getResource("sample.fxml"));
            BorderPane root;
            root = loader.load();
            // set a whitesmoke background
            root.setStyle("-fx-background-color: whitesmoke;");
            // create and style a scene
            Scene scene = new Scene(root, 800, 600);
            scene.getStylesheets().add(getClass().getResource("/fxml/application.css").toExternalForm());
            // create the stage with the given title and the previously created
            // scene
            primaryStage.setTitle("Reconnaissance Faciale");
            primaryStage.setScene(scene);
            // show the GUI
            primaryStage.show();

            // init the controller
            Controller controller = loader.getController();
            controller.init();

            // set the proper behavior on closing the application
            primaryStage.setOnCloseRequest((new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we)
                {
                    controller.setClosed();
                }
            }));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException
    {
        FileInputStream serviceAccount = new FileInputStream("src/main/resources" +
                "/gosecuri-58a5c-firebase-adminsdk-vem79-ce2ec661f9.json");

//        FirestoreOptions options2 =
//                FirestoreOptions.newBuilder().setTimestampsInSnapshotsEnabled(true).build();
//        Firestore firestore = options2.getService();

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl("https://gosecuri-58a5c.firebaseio.com")
                .build();

        FirebaseApp.initializeApp(options);

//        Firestore db = FirestoreClient.getFirestore();

        //FirebaseFirestore.setLoggingEnabled(true);

//        DocumentReference docRef = db.collection("users").document("aturing");
//        // Add document data with an additional field ("middle")
//        Map<String, Object> data = new HashMap<>();
//        data.put("first", "Alan");
//        data.put("middle", "Mathison");
//        data.put("last", "Turing");
//        data.put("born", 1912);

//        ApiFuture<WriteResult> result = docRef.set(data);
//        System.out.println("Update time : " + result.get().getUpdateTime());

        // asynchronously retrieve all users
//        ApiFuture<QuerySnapshot> query = db.collection("users").get();
        // ...
        // query.get() blocks on response
//        QuerySnapshot querySnapshot = query.get();
//        List<QueryDocumentSnapshot> documents = querySnapshot.getDocuments();
//        for(QueryDocumentSnapshot document : documents) {
//            System.out.println("User: " + document.getId());
//            System.out.println("First: " + document.getString("first"));
//            if (document.contains("middle")) {
//                System.out.println("Middle: " + document.getString("middle"));
//            }
//            System.out.println("Last: " + document.getString("last"));
//            System.out.println("Born: " + document.getLong("born"));
//        }

        // Reference to a document in subcollection "messages"
//        DocumentReference document2 =
//                db.collection("rooms").document("roomA")
//                        .collection("messages").document("message1");

        // load the native OpenCV library
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        launch(args);
    }
}
