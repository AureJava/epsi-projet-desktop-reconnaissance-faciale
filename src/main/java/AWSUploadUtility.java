import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

import java.io.InputStream;

public class AWSUploadUtility {
    public void uploadfile(AWSCredentials credentials, String bucketName, String keyName, InputStream is, ObjectMetadata metadata)
    {
        AmazonS3 s3client = new AmazonS3Client(credentials);
        try{
            System.out.println("uploading a new object to S3 from a file\n");
            PutObjectRequest putObj=new PutObjectRequest(bucketName,keyName,is,metadata);
            putObj.setCannedAcl(CannedAccessControlList.PublicRead);
            s3client.putObject(putObj);
        }
        catch(AmazonServiceException ase){
            System.out.println("exception AmazonServiceException lequel"+
                    "signifie que c'est ta requête"+
                    "a Amazon S3 mais a été rejeté avec une réponse d'erreur");
            System.out.println("AWS Error Code: "+ase.getErrorCode());
        }
        catch(AmazonClientException ace)
        {
            System.out.println("AmazonClientException signifie"+
                    "que le client a rencontré une erreur interne"+
                    "pendant éssayant de communiquer avec S3,"+
                    "pas capable d'accéder aux réseaux");
            System.out.println("Erreur message: "+ ace.getMessage());
        }
    }
}
