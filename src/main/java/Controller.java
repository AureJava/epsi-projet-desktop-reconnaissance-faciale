import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.Map.Entry;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.*;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClientBuilder;
import com.amazonaws.services.rekognition.model.*;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.util.IOUtils;
import com.sun.xml.internal.ws.client.ResponseContext;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileItemFactory;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.RequestContext;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacv.OpenCVFrameGrabber;
import org.openimaj.feature.FloatFV;
import org.openimaj.feature.FloatFVComparison;
import org.openimaj.image.DisplayUtilities;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.processing.face.detection.HaarCascadeDetector;
import org.openimaj.image.processing.face.detection.keypoints.FKEFaceDetector;
import org.openimaj.image.processing.face.detection.keypoints.KEDetectedFace;
import org.openimaj.image.processing.face.feature.FacePatchFeature;
import org.openimaj.image.processing.face.feature.FacePatchFeature.Extractor;
import org.openimaj.image.processing.face.feature.comparison.FaceFVComparator;
import org.openimaj.image.processing.face.similarity.FaceSimilarityEngine;
import org.openimaj.math.geometry.shape.Rectangle;

import java.io.File;


import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamResolution;
import com.typesafe.config.ConfigFactory;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;
import org.opencv.videoio.VideoCapture;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static akka.pattern.Patterns.ask;
import static org.bytedeco.javacpp.opencv_imgcodecs.cvSaveImage;
import static scala.concurrent.Await.result;

/**
 * The controller associated with the only view of our application. The
 * application logic is implemented here. It handles the button for
 * starting/stopping the camera, the acquired video stream, the relative
 * controls and the face detection/tracking.
 *
 * @author <a>Aurelien</a>
 * @version 1.1 (2015-11-10)
 * @since 1.0 (2014-01-10)
 *
 */
public class Controller extends HttpServlet implements Initializable{

    private static final long serialVerisonUID = 1L;

    // FXML buttons
    @FXML
    private Button cameraButton;
    @FXML
    private Button identificationButton;
    // the FXML area for showing the current frame
    @FXML
    private ImageView originalFrame;
    @FXML
    private ImageView localaccessFrame;
    // checkboxes for enabling/disabling a classifier
    @FXML
    private CheckBox haarClassifier;
    @FXML
    private CheckBox lbpClassifier;
/*    @FXML
    private CheckBox[] ch1;*/
    @FXML
    private CheckBox ch1;
    @FXML
    private CheckBox ch2;
    @FXML
    private CheckBox ch3;
    @FXML
    private CheckBox ch4;
    @FXML
    private CheckBox ch5;
    @FXML
    private CheckBox ch6;
    @FXML
    private CheckBox ch7;
    @FXML
    private CheckBox ch8;
    @FXML
    private CheckBox ch9;
    @FXML
    private CheckBox ch10;
    @FXML
    private CheckBox ch11;
    @FXML
    private CheckBox ch12;
    @FXML
    private CheckBox ch13;
    @FXML
    private CheckBox ch14;
    @FXML
    private AnchorPane rootPane;


    static enum GetImageMsg {
        OBJECT;
    }

    static class WebcamActor extends UntypedActor {

        final Webcam webcam;

        public WebcamActor(Webcam webcam) {
            this.webcam = webcam;
        }

        @Override
        public void preStart() throws Exception {
            webcam.setViewSize(WebcamResolution.VGA.getSize());
            webcam.open();
        }

        @Override
        public void postStop() throws Exception {
            webcam.close();
        }

        @Override
        public void onReceive(Object msg) throws Exception {
            if (msg instanceof GetImageMsg) {
                sender().tell(getImage(), self());
            } else {
                unhandled(msg);
            }
        }

        public BufferedImage getImage() {
            return webcam.getImage();
        }
    }

    // a timer for acquiring the video stream
    private ScheduledExecutorService timer;
    // the OpenCV object that performs the video capture
    private VideoCapture capture;
    // a flag to change the button behavior
    private boolean cameraActive;

    // face cascade classifier
    private CascadeClassifier faceCascade;
    private int absoluteFaceSize;

 //   public Controller() {
  //  }

    public void init() {
        this.capture = new VideoCapture();
        this.faceCascade = new CascadeClassifier();
        this.absoluteFaceSize = 0;

        // set a fixed width for the frame
        originalFrame.setFitWidth(600);
        // preserve image ratio
        originalFrame.setPreserveRatio(true);

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    private void LoadingFaceTrackingSystem(String classifierPath) {
        // load the classifier(s)
        this.faceCascade.load(classifierPath);

        // now the video capture can start
        this.cameraButton.setDisable(false);
    }

    /**
     * The action triggered by pushing the button on the GUI
     */
    @FXML
    protected void startCamera() {
        if (!this.cameraActive) {
            // disable setting checkboxes
            //this.haarClassifier.setDisable(false);
//            this.lbpClassifier.setDisable(true);

            // start the video capture
            this.capture.open(0);

            // is the video stream available?
            if (this.capture.isOpened()) {
                this.cameraActive = true;

                // grab a frame every 33 ms (30 frames/sec)
                Runnable frameGrabber = new Runnable() {

                    @Override
                    public void run() {
                        // effectively grab and process a single frame
                        Mat frame = grabFrame();
                        // convert and show the frame
                        Image imageToShow = Utils.mat2Image(frame);
                        updateImageView(originalFrame, imageToShow);
                    }
                };

                this.timer = Executors.newSingleThreadScheduledExecutor();
                this.timer.scheduleAtFixedRate(frameGrabber, 0, 33, TimeUnit.MILLISECONDS);

                this.LoadingFaceTrackingSystem("C:\\Users\\User\\Documents\\EPSI\\GoSecuri\\src\\main\\resources\\fxml\\haarcascade_frontalface_alt.xml");

                // update the button content
                this.cameraButton.setText("Stop Camera");
            } else {
                // log the error
                System.err.println("Echec à ouvrir la connection camera...");
            }
        } else {
            // the camera is not active at this point
            this.cameraActive = false;
            // update again the button content
            this.cameraButton.setText("Start Camera");
            // enable classifiers checkboxes
//          this.haarClassifier.setDisable(false);
//          this.lbpClassifier.setDisable(false);

            // stop the timer
            this.stopAcquisition();
        }
    }

    /**
     * Get a frame from the opened video stream (if any)
     *
     * @return the {@link Image} to show
     */
    private Mat grabFrame() {
        Mat frame = new Mat();

        // check if the capture is open
        if (this.capture.isOpened()) {
            try {
                // read the current frame
                this.capture.read(frame);

                // if the frame is not empty, process it
                if (!frame.empty()) {
                    // face detection
                    this.detectAndDisplay(frame);
                }

            } catch (Exception e) {
                // log the (full) error
                System.err.println("Exception durant l'elaboation de l'image: " + e);
            }
        }

        return frame;
    }

    /**
     * Method for face detection and tracking
     *
     * @param frame it looks for faces in this frame
     */
    private void detectAndDisplay(Mat frame) {
        MatOfRect faces = new MatOfRect();
        Mat grayFrame = new Mat();

        // convert the frame in gray scale
        Imgproc.cvtColor(frame, grayFrame, Imgproc.COLOR_BGR2GRAY);
        // equalize the frame histogram to improve the result
        Imgproc.equalizeHist(grayFrame, grayFrame);

        // compute minimum face size (20% of the frame height, in our case)
        if (this.absoluteFaceSize == 0) {
            int height = grayFrame.rows();
            if (Math.round(height * 0.2f) > 0) {
                this.absoluteFaceSize = Math.round(height * 0.2f);
            }
        }

        // detect faces
        this.faceCascade.detectMultiScale(grayFrame, faces, 1.1, 2, 0 | Objdetect.CASCADE_SCALE_IMAGE,
                new Size(this.absoluteFaceSize, this.absoluteFaceSize), new Size());

        // each rectangle in faces is a face: draw them!
        Rect[] facesArray = faces.toArray();
        for (int i = 0; i < facesArray.length; i++)
            Imgproc.rectangle(frame, facesArray[i].tl(), facesArray[i].br(), new Scalar(0, 255, 0), 3);

    }

    /**
     * Stop the acquisition from the camera and release all the resources
     */
    private void stopAcquisition() {
        if (this.timer != null && !this.timer.isShutdown()) {
            try {
                // stop the timer
                this.timer.shutdown();
                this.timer.awaitTermination(33, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                // log any exception
                System.err.println("Exception en arrêtant le cadre de capture,libere la camera... " + e);
            }
        }

        if (this.capture.isOpened()) {
            // release the camera
            this.capture.release();
        }
    }

    /**
     * Update the {@link ImageView} in the JavaFX main thread
     *
     * @param view  the {@link ImageView} to update
     * @param image the {@link Image} to show
     */
    private void updateImageView(ImageView view, Image image) {
        Utils.onFXThread(view.imageProperty(), image);
    }

    /**
     * On application close, stop the acquisition from the camera
     */
    protected void setClosed() {
        this.stopAcquisition();
    }

    @FXML
    public void Identification(ActionEvent actionEvent) throws IOException {
        try {
            final ActorSystem system = ActorSystem.create("hello-from-akka", ConfigFactory.defaultApplication());
            final ActorRef ref = system.actorOf(Props.create(WebcamActor.class, Webcam.getDefault()));

            final File file = new File("test.jpg");
            final Duration timeout = FiniteDuration.create("10s");
            final BufferedImage image = (BufferedImage) result(ask(ref, GetImageMsg.OBJECT, timeout.toMillis()), timeout);

            ImageIO.write(image, "JPG", file);

            JOptionPane.showMessageDialog(null, "Image has been saved in file: " + file);

            system.terminate();


            String existingBucketName = "gosecurirepository";
            String keyName = "test.jpg";

            String filePath = "C:\\Users\\User\\Documents\\EPSI\\GoSecuri\\test.jpg";
            String amazonFileUploadLocationOriginal = existingBucketName + "/";

            AmazonS3 s3Client = new AmazonS3Client(new PropertiesCredentials(Controller.class.getResourceAsStream
                    ("AwsCredentials.properties")));

            FileInputStream stream = new FileInputStream(filePath);
            ObjectMetadata objectMetadata = new ObjectMetadata();
            PutObjectRequest putObjectRequest = new PutObjectRequest(amazonFileUploadLocationOriginal, keyName, stream, objectMetadata);
            PutObjectResult result = s3Client.putObject(putObjectRequest);
            System.out.println("Etag:" + result.getETag() + "-->" + result);


            // first, we load two images
//            final URL image1url = new URL(
//                    "https://gosecurirepository.s3.eu-west-3.amazonaws.com//test.jpg");
//            final URL image2url = new URL(
//                    "gs://gosecuri-58a5c.appspot.com/photocomparaison.jpg");
//
//            final FImage image1 = ImageUtilities.readF(image1url);
//            final FImage image2 = ImageUtilities.readF(image2url);
//
//            // then we set up a face detector; will use a haar cascade detector to
//            // find faces, followed by a keypoint-enhanced detector to find facial
//            // keypoints for our feature. There are many different combinations of
//            // features and detectors to choose from.
//            final HaarCascadeDetector detector = HaarCascadeDetector.BuiltInCascade.frontalface_alt2.load();
//            final FKEFaceDetector kedetector = new FKEFaceDetector(detector);
//
//            // now we construct a feature extractor - this one will extract pixel
//            // patches around prominant facial keypoints (like the corners of the
//            // mouth, etc) and build them into a vector.
//            final Extractor extractor = new FacePatchFeature.Extractor();
//
//            // in order to compare the features we need a comparator. In this case,
//            // we'll use the Euclidean distance between the vectors:
//            final FaceFVComparator<FacePatchFeature, FloatFV> comparator =
//                    new FaceFVComparator<FacePatchFeature, FloatFV>(FloatFVComparison.EUCLIDEAN);
//
//            // Now we can construct the FaceSimilarityEngine. It is capable of
//            // running the face detector on a pair of images, extracting the
//            // features and then comparing every pair of detected faces in the two
//            // images:
//            final FaceSimilarityEngine<KEDetectedFace, FacePatchFeature, FImage> engine =
//                    new FaceSimilarityEngine<KEDetectedFace, FacePatchFeature, FImage>(kedetector, extractor, comparator);
//
//            // we need to tell the engine to use our images:
//            engine.setQuery(image1, "image1");
//            engine.setTest(image2, "image2");
//
//            // and then to do its work of detecting, extracting and comparing
//            engine.performTest();
//
//            // finally, for this example, we're going to display the "best" matching
//            // faces in the two images. The following loop goes through the map of
//            // each face in the first image to all the faces in the second:
//            for (final Entry<String, Map<String, Double>> e : engine.getSimilarityDictionary().entrySet()) {
//                // this computes the matching face in the second image with the
//                // smallest distance:
//                double bestScore = Double.MAX_VALUE;
//                String best = null;
//                for (final Entry<String, Double> matches : e.getValue().entrySet()) {
//                    if (matches.getValue() < bestScore) {
//                        bestScore = matches.getValue();
//                        best = matches.getKey();
//                    }
//                }

//                // and this composites the original two images together, and draws
//                // the matching pair of faces:
//                final FImage img = new FImage(image1.width + image2.width, Math.max(image1.height, image2.height));
//                img.drawImage(image1, 0, 0);
//                img.drawImage(image2, image1.width, 0);
//
//                img.drawShape(engine.getBoundingBoxes().get(e.getKey()), 1F);
//
//                final Rectangle r = engine.getBoundingBoxes().get(best);
//                r.translate(image1.width, 0);
//                img.drawShape(r, 1F);
//
//                // and finally displays the result
//                DisplayUtilities.display(img);
//            }

            /*AnchorPane pane= FXMLLoader.load(getClass().getResource("/sample/localsystem.fxml"));
            rootPane.getChildren().setAll(pane);*/
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/localsystem.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setTitle("Système de gestion du local");
            stage.setScene(new Scene(root));
            stage.show();
        } catch (Exception e) {
            Parent root = FXMLLoader.load(getClass().getResource("/fxml/popup.fxml"));
            Scene scene=new Scene(root);
            Stage stage=new Stage();
            stage.setTitle("Popup warning");
            stage.setScene(scene);
            stage.show();
            //System.out.println("On ne peut pas charger la nouvelle interface");
        }
    }
    @FXML
    public void checkEvent(ActionEvent checkEvent) {
        int count1=0;
        int count2=0;
        int count3=0;
        int count4=0;
        int count5=0;
        int count6=0;
        int count7=0;
        int count8=0;
        int count9=0;
        int count10=0;
        int count11=0;
        int count12=0;
        int count13=0;
        int count14=0;


        //CheckBox[] mousquetonCheckBox = new CheckBox[maxSel.length]; ;
        int maxSel1 = 15;
        int maxSel2 = 10;
        int maxSel3 = 20;
        int maxSel4 = 25;
        int maxSel5 = 30;
        int maxSel6 = 5;
        int maxSel7 = 5;
        int maxSel8 = 12;
        int maxSel9 = 30;
        int maxSel10 = 30;
        int maxSel11 = 30;
        int maxSel12 = 20;
        int maxSel13 = 10;
        int maxSel14 = 5;

       /* for (int i = 0; i < this.ch1.length; i++)
            this.ch1[i].selectedProperty().addListener((o, oldV, newV) -> {
                if (newV) {
                    int sel = 0;
                    for (CheckBox cb : ch1) {
                        if (cb.isSelected()) {
                            sel++;
                            if (sel == maxSel1) {
                                cb.setDisable(true);
                            if(cb.isSelected()){
                                cb.setDisable(false);
                                sel--;
                            }
                            }

                        }
                    }
                }
            });*/
        if(ch1.isSelected())
        {
            count1++;
            if (count1==maxSel1) {
                ch1.setDisable(true);
                if (ch1.isSelected()) {
                    ch1.setDisable(false);
                    count1--;
                }
            }
        }
        if(ch2.isSelected())
        {
            count2++;
            if (count2==maxSel2) {
                ch2.setDisable(true);
                if (ch2.isSelected()) {
                    ch2.setDisable(false);
                    count2--;
                }
            }
        }
        if(ch3.isSelected())
        {
            count3++;
            if (count3==maxSel3) {
                ch3.setDisable(true);
                if (ch3.isSelected()) {
                    ch3.setDisable(false);
                    count3--;
                }
            }
        }
        if(ch4.isSelected())
        {
            count4++;
            if (count4==maxSel4) {
                ch4.setDisable(true);
                if (ch4.isSelected()) {
                    ch4.setDisable(false);
                    count4--;
                }
            }
        }
        if(ch5.isSelected())
        {
            count5++;
            if (count5==maxSel5) {
                ch5.setDisable(true);
                if (ch5.isSelected()) {
                    ch5.setDisable(false);
                    count5--;
                }
            }
        }
        if(ch6.isSelected())
        {
            count6++;
            if (count6==maxSel6) {
                ch6.setDisable(true);
                if (ch6.isSelected()) {
                    ch6.setDisable(false);
                    count6--;
                }
            }
        }
        if(ch7.isSelected())
        {
            count7++;
            if (count7==maxSel7) {
                ch7.setDisable(true);
                if (ch7.isSelected()) {
                    ch7.setDisable(false);
                    count7--;
                }
            }
        }
        if(ch8.isSelected())
        {
            count8++;
            if (count8==maxSel8) {
                ch8.setDisable(true);
                if (ch8.isSelected()) {
                    ch8.setDisable(false);
                    count8--;
                }
            }
        }
        if(ch9.isSelected())
        {
            count9++;
            if (count9==maxSel9) {
                ch9.setDisable(true);
                if (ch9.isSelected()) {
                    ch9.setDisable(false);
                    count9--;
                }
            }
        }
        if(ch10.isSelected())
        {
            count10++;
            if (count10==maxSel10) {
                ch10.setDisable(true);
                if (ch10.isSelected()) {
                    ch10.setDisable(false);
                    count10--;
                }
            }
        }
        if(ch11.isSelected())
        {
            count11++;
            if (count11==maxSel11) {
                ch11.setDisable(true);
                if (ch11.isSelected()) {
                    ch11.setDisable(false);
                    count11--;
                }
            }
        }
        if(ch12.isSelected())
        {
            count12++;
            if (count12==maxSel12) {
                ch12.setDisable(true);
                if (ch12.isSelected()) {
                    ch12.setDisable(false);
                    count12--;
                }
            }
        }
        if(ch13.isSelected())
        {
            count13++;
            if (count13==maxSel13) {
                ch13.setDisable(true);
                if (ch13.isSelected()) {
                    ch13.setDisable(false);
                    count13--;
                }
            }
        }
        if(ch14.isSelected())
        {
            count14++;
            if (count14==maxSel14) {
                ch14.setDisable(true);
                if (ch14.isSelected()) {
                    ch14.setDisable(false);
                    count14--;
                }
            }
        }
    }

    @FXML
    public void RetourMain(ActionEvent retourEvent) {
    }


}